# AWS_INFRA_LOCALSTACK

-First to install localstack we need to have docker in our system after that run below command 
`
`docker run -it -p 4500-4600:4500-4600 -p 8080:8080 --expose 4572 localstack/localstack:0.11.1`

- Go to the existing repo and perform terrform commands

`terrform init`
`terraform validate`
`terraform plan`
`terraform apply`

If you want to validate the services deployed into localtack use below command 
`aws --endpoint-url=http://localhost:4566 ec2 describe-instances --region us-east-1`

Depends on aws services url and service name will change accordingly


After verification you can destroy your AWS services
`terraform destroy`



