module "ec2useast1" {
  source        = "./modules/ec2"
  region = "us-east-1"
  instance_type = var.instance_type
  subnet_id        = module.vpc.subnet_id
  security_group_id = module.securitygroups.security_group_id


}
module "vpc" {
  source        = "./modules/vpc"
  region = "us-east-1"
}
module "securitygroups" {
  source        = "./modules/securitygroups"
  region = "us-east-1"
  vpc_id = module.vpc.vpc_id
}

module "ec2useast2" {
  source        = "./modules/ec2"
  providers     = {
    aws = aws.useast2
  }
  region = "us-east-2"
  instance_type = var.instance_type
 subnet_id        = module.vpc.subnet_id
  security_group_id = module.securitygroups.security_group_id
}
module "vpcuseast2" {
  source        = "./modules/vpc"
  providers     = {
    aws = aws.useast2
  }
  region = "us-east-2"
}
module "securitygroupsuseast2" {
  source        = "./modules/securitygroups"
  vpc_id = module.vpc.vpc_id
   providers     = {
    aws = aws.useast2
  }
  region = "us-east-2"
}