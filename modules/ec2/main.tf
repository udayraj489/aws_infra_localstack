resource "aws_instance" "main" {
  ami           = "ami-0d57c0143330e1fa7" # This should be a valid AMI in your LocalStack configuration
  instance_type = var.instance_type
  subnet_id     = var.subnet_id
  security_groups = [var.security_group_id]


  # tags = {
  #   Name = "${var.region}-instance"
  # }
}